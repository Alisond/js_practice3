   //Numero aleatorio
   let maxValue = prompt ('Ingresa el número máximo');
   let secretNumber = randomSecretNumber(maxValue);
   
   //Inicializar intentos
   let userMaxAtempts = parseInt(prompt ('Ingresa el máximo de intentos que quieres tener disponibles'));
   let count = 1;

initialization();

//Functions
 function  textAsing(element, text){
   let elementHMTL = document.querySelector(element);
   elementHMTL.innerHTML = text;
   return;
 }

 function verifyAtempts(){
   let userAtempt = parseInt(document.getElementById('userNumber').value);
   if(userMaxAtempts >= 1){
      if(userAtempt === secretNumber){
         textAsing('p', `Acertaste el número en ${count} ${count === 1 ? 'Intento' : 'Intentos'}. El número secreto era: ${secretNumber}`);
         document.getElementById('reiniciar').removeAttribute('disabled');
      } else {
         count++;
         userMaxAtempts--;
         textAsing('p',`Inténtalo de nuevo con un número ${userAtempt < secretNumber ? 'mayor' : 'menor'}, intentos disponibles ${userMaxAtempts}`)
         cleanBox();   
      }
   }else {
      textAsing('p', 'Lástima, no te quedan más intentos. Reinicia para jugar de nuevo.');
   }
 }

 function cleanBox(){
   document.querySelector('#userNumber').value = '';
 }

 function randomSecretNumber(maxValue){
   return Math.floor(Math.random()*maxValue)+ 1;
 }

 function initialization(){
   //Iniciar textos
   textAsing('h1', 'Guest the secret number');
   textAsing('p', `Enter a number between 1 and ${maxValue}`);

 }

 function replay(){
   //limpiar caja
   cleanBox(); 

   //Inhabilitar el botón
   document.getElementById('reiniciar').setAttribute('disabled', true);

   initialization();
 }